// ALWAYS CHECK THAT THE DIP SWITCHES ARE SET TO WHAT WE 
// WANT THE MICROSTEPPING TO BE, BELOW IN SETUP

// THESE MOTORS DO NOT LIKE BEING RUN AT 12V
// RUN THEM AT AT LEAST 15V OR SO

// TODO
// Setup so ESP saves settings into flash, restores on startup

// For logging capabilities "with minimal performance cost"
// See: https://github.com/thijse/Arduino-Log/
#include <ArduinoLog.h>

// For I2C
#include <Wire.h>

// What's the I2C address of the guide
#define I2CAddressGuide 8

// What is our log level?
#define log_level LOG_LEVEL_VERBOSE

// Define the output pins
// D0, D1 imply that we're doing this on an ESP8266 or something of that sort
// Can easily switch out to an Arduino just by changing the pins to something like
// 2 or 3
#define dirPin D0 // connect to DIR+
#define stepPin D1 // connect to PUL+
//#define speedPot A0 // which analog in is the speed pot connected to

// TODO
// setup I2C to communicate with ESP back and forth
// Start looking here: https://forum.arduino.cc/index.php?topic=419711.0
// https://medium.com/@krukmat/arduino-esp8266-through-i2c-49b78e697b7a

/*
 * Switches for microstepping
 * !!!VALID FOR TB6600 MOTOR CONROL BOARD ONLY!!!
 * something like this: 
 * https://www.amazon.com/TB6600-Stepper-Driver-Controller-tb6600/dp/B07S64MBTR/
 * 
 * See also:
 * https://www.makerguides.com/tb6600-stepper-motor-driver-arduino-tutorial/
 * 
 * 200  ON  ON  OFF
 * 400  ON  OFF ON
 * 800  ON  OFF OFF
 * 1600 OFF ON  OFF
 * 3200 OFF OFF ON
 * 6400 OFF OFF OFF
 * 
 */

// Are we microstepping?
int microstepping = HIGH;

// Setup enum to make the code easier to read
// ALSO CHANGE THIS WHEN YOU CHANGE THE MICROSTEP STATE
enum mircrostepStates {
  MICRO_200 = 1,
  MICRO_400 = 2,
  MICRO_800 = 4,
  MICRO_1600 = 8,
  MICRO_3200 = 16,
  MICRO_6400 = 32
} microstepState = MICRO_6400;

// How many steps per revolution? Get this from the datasheet
// (360 / angle per step)
long stepsPerRevolution = 200;

// What is the delay time? This will change depending on desired speed and 
// microstepping state and will be calculated below
int delayTimeMicros = 500; // delay time in microseconds

// What is our desired RPMs?
long rpms = 60;

void setup() {
  // Setup logging
  Serial.begin(9600);
  while(!Serial && !Serial.available()){}
  Log.begin(log_level, &Serial);
  Log.verbose("Beginning setup" CR);

  // Setup the pins
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  digitalWrite(dirPin, LOW);
  Log.verbose("Pins set" CR);

  // Setup I2C
  //Wire.begin(I2CAddressGuide);
  //Wire.onReceive(guideReceiveEvent);
  //Wire.onRequest(guideRequestEvent);
  //Log.verbose("Setup I2C, address of guide is %d" CR, I2CAddressGuide);

  // Calculate the delay time given our parameters
  Log.verbose("Setting delay time in microseconds" CR);
  
  if (microstepping) {
    delayTimeMicros = getDelayMicros(rpms, stepsPerRevolution, microstepState);
  } else {
    delayTimeMicros = getDelayMicros(rpms, stepsPerRevolution);
  }
  
  Log.notice("delayTimeMicros: %d" CR, delayTimeMicros);
  
  Log.verbose("Ending setup" CR);
}

void loop() {
  // Check if we need to update the speed setting or not
  
  // Run through our usual loop of stepping
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(delayTimeMicros);
  digitalWrite(stepPin, LOW);
  delayMicroseconds(delayTimeMicros);
}

// Get the desired delay based on these parameters
int getDelayMicros(long rpm, long stepsPerRev, int microState) {
  Log.notice("Values: %d, %d, %d" CR, rpm, stepsPerRev, microState);

  float delayTimeFloat = 60L * 1000L * 1000L / (2 * rpm * stepsPerRev * microState);
  
  Log.notice("delayTimeFloat: %F" CR, delayTimeFloat);
  long delayTime = round(delayTimeFloat);

  return delayTime;
}


// Get the desired delay based on these parameters
int getDelayMicros(long rpm, long stepsPerRev) {
  Log.notice("Values: %d, %d" CR, rpm, stepsPerRev);
  
  float delayTimeFloat = 60L * 1000L * 1000L / (2 * rpm * stepsPerRev);
  
  Log.notice("delayTimeFloat: %F" CR, delayTimeFloat);
  long delayTime = round(delayTimeFloat);

  return delayTime;
}

void guideReceiveEvent(int count) {
  Log.notice("Received: " CR);
  while (Wire.available()) {
    byte c = Wire.read();
    Log.notice("Received value: %d" CR, c);
  }
}

/*
void guideRequestEvent() {
  char c = "!";
  Wire.write(c); 
}
*/
