# Clinostat

Code and schematics for open clinostat.

This is part of the [tranxxeno lab](https://tranxxenolab.net).

# Credits

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance.

Produced by Galerija Kapelica/Zavod Kersnikova in Ljubljana, Slovenia.

Biofriction is supported by the European Commission–Creative Europe.
